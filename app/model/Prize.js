const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const Prize = new Schema({
	// _id: {type: ObjectId, unique:true},
	name: {type: String, required:true, unique: true},
	description: {type: String, required:false},
},{
	timestamps:true
});

module.exports = mongoose.model('Prize', Prize);
 