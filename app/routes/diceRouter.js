// Khai báo thư viện express
const express = require('express');

// Import Middleware
const { diceMiddleware } = require('../middlewares/diceMiddleware');


//Import  Controller
const { diceHandler, getDiceHistoryByUsername, getPrizeHistoryByUsername, getVoucherHistoryByUsername } = require('../controllers/diceController');

// Tạo router
const diceRouter = express.Router();

// Sử dụng middleware
diceRouter.use(diceMiddleware);


// KHAI BÁO API

diceRouter.post('/devcamp-lucky-dice/dice', diceHandler)


//devcamp-lucky-dice/dice-history?username=:username
diceRouter.get("/devcamp-lucky-dice/dice-history", getDiceHistoryByUsername);


//devcamp-lucky-dice/prize-history?username=:username
diceRouter.get("/devcamp-lucky-dice/prize-history", getPrizeHistoryByUsername);


//devcamp-lucky-dice/prize-history?username=:username
diceRouter.get("/devcamp-lucky-dice/voucher-history", getVoucherHistoryByUsername);

// EXPORT ROUTER
module.exports = {diceRouter};