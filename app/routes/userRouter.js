//khai báo thư viện express
const express = require('express');
const userMiddleware = require('../middlewares/userMiddleware');
const userController = require('../controllers/userController');
//tạo router
const userRouter = express.Router();

//sủ dụng middle ware
userRouter.use(userMiddleware);

//get all users
userRouter.get('/users', userController.getAllUser);

//get a user
userRouter.get('/users/:userid', userController.getUserById);

//create a user
userRouter.post('/users',userController.createUser);

//update a user
userRouter.put('/users/:userid',userController.updateUserById);

//delete a user
userRouter.delete('/users/:userid',userController.deleteUserById);

module.exports = { userRouter };