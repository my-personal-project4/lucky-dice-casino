//khai báo thư viện express
const express = require('express');
const voucherMiddleware = require('../middlewares/voucherMiddleware');
const voucherController = require('../controllers/voucherController');
//tạo router
const voucherRouter = express.Router();

//sủ dụng middle ware
voucherRouter.use(voucherMiddleware);

//get all vouchers
voucherRouter.get('/vouchers', voucherController.getAllVoucher);

//get a voucher
voucherRouter.get('/vouchers/:voucherid', voucherController.getVoucherById);

//create a voucher
voucherRouter.post('/vouchers', voucherController.createVoucher);

//update a voucher
voucherRouter.put('/vouchers/:voucherid',voucherController.updateVoucherById);

//delete a voucher
voucherRouter.delete('/vouchers/:voucherid',voucherController.deleteVoucherById);

module.exports = { voucherRouter };