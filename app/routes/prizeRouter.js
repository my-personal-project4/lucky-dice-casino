//khai báo thư viện express
const express = require('express');
const prizeMiddleware = require('../middlewares/prizeMiddleware');
const prizeController = require('../controllers/prizeController');
//tạo router
const prizeRouter = express.Router();

//sủ dụng middle ware
prizeRouter.use(prizeMiddleware);

//get all Prizes
prizeRouter.get('/prizes', prizeController.getAllPrize);

//get a Prize
prizeRouter.get('/prizes/:prizeid', prizeController.getPrizeById);

//create a Prize
prizeRouter.post('/prizes', prizeController.createPrize);

//update a Prize
prizeRouter.put('/prizes/:prizeid', prizeController.updatePrizeById);

//delete a Prize

prizeRouter.delete('/prizes/:prizeid',prizeController.deletePrizeById);

module.exports = { prizeRouter };