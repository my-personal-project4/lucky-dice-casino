const Voucher = require('../model/Voucher');
//import thư viện mongoose
const mongoose = require('mongoose');
class VoucherController {
  getAllVoucher(req,res) {
    Voucher.find((error,data)=> {
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Get all vouchers successfully",
        vouchers: data
      })
    })
  }

  createVoucher(req,res){
    //B1: thu thập dữ liệu từ req
    let body = req.body;
    // console.log(body);
    //B2: validate dữ liệu
    if(!body.code) {
      return res.status(400).json({
        message: 'Mã voucher phải nhập'
      })
    }
    if(!Number.isInteger(body.discount) || body.discount < 0){
      return res.status(400).json({
        message: 'Phần trăm giảm giá không hợp lệ'
      }) 
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newVoucherData = {
      _id: mongoose.Types.ObjectId(),
      code: body.code,
      discount: body.discount,
      note: body.note
    }
    Voucher.create(newVoucherData, (error,data) =>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(201).json({
        message: "Create successfully",
        newVoucher: data
      })
    })
  }

  getVoucherById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.voucherid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'VoucherId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    Voucher.findById(id, (error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(200).json({
        message: "Get voucher successfully",
        voucher: data
      })
    })  
  }

  updateVoucherById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.voucherid;
    let body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'VoucherId is invalid!'
      })
    }
    //bóc tách trường hợp undefined
    if(body.code !== undefined && body.code =="") {
      return res.status(400).json({
        message: 'Mã voucher phải nhập'
      })
    }
    if(body.discount !== undefined &&(!Number.isInteger(body.discount) || body.discount < 0)){
      return res.status(400).json({
        message: 'Phần trăm giảm giá không hợp lệ'
      }) 
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let voucherUpdate = {
      code: body.code,
      discount: body.discount,
      note: body.note
    }
    if(body.code){
      voucherUpdate.code = body.code;
    }
    if(body.discount){
      voucherUpdate.discount = body.discount;
    }
    if(body.note){
      voucherUpdate.note = body.note;
    }

    Voucher.findByIdAndUpdate(id,voucherUpdate,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Update voucher successfully",
        voucher: data
      })
    })
  }

  deleteVoucherById(req,res){
    //B1: thu thập dữ liệu từ req
    let id = req.params.voucherid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'VoucherId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    Voucher.findByIdAndDelete(id,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(204).json({
        message: "Delete voucher successfully",
      })
    })
  }
} 
module.exports = new VoucherController;