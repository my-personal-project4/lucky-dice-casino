const PrizeHistory = require('../model/prizeHistory');
//import thư viện mongoose
const mongoose = require('mongoose');

class PrizeHistoryController {
  //
  getAllPrizeHistory(req,res) {
    //B1: thu thập dữ liệu từ req
    // let id = req.query.user
    let {user} = req.query
    let condition = {};
    if(user){
        condition.user = user;
    }
    //B2: validate dữ liệu
    // if(!mongoose.Types.ObjectId.isValid(id)) {
    //   return res.status(400).json({
    //     message: 'UserId is invalid!'
    //   })
    // }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    PrizeHistory.find(condition)
    .populate('user')
    .populate('prize')
    .exec((error,data)=> {
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Get all PrizeHistorys successfully",
        PrizeHistories: data
      })
    })
  }

  createPrizeHistory(req,res){
    //B1: thu thập dữ liệu từ req
    let body = req.body;
    // console.log(body);
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
      return res.status(400).json({
        message: 'User is invalid!'
      })
    }
    if(!mongoose.Types.ObjectId.isValid(body.prize)) {
      return res.status(400).json({
        message: 'Prize is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newPrizeHistoryData = {
      _id: mongoose.Types.ObjectId(),
      user: body.user,
      prize: body.prize,
    }
    PrizeHistory.create(newPrizeHistoryData, (error,data) =>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(201).json({
        message: "Create successfully",
        newPrizeHistory: data
      })
    })
  }

  getPrizeHistoryById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.historyId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'PrizeHistoryId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    PrizeHistory.findById(id)
    .populate('user')
    .populate('prize')
    .exec((error,data)=> {
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(200).json({
        message: "Get PrizeHistory successfully",
        PrizeHistory: data
      })
    })
  }

  updatePrizeHistoryById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.historyId;
    let body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'PrizeHistoryId is invalid!'
      })
    }
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
      return res.status(400).json({
        message: 'User is invalid!'
      })
    }
    if(!mongoose.Types.ObjectId.isValid(body.prize)) {
      return res.status(400).json({
        message: 'Prize is invalid!'
      })
    }
    //bóc tách trường hợp undefined
    
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let prizeHistoryUpdate = {
      user: body.user,
      prize: body.prize
    }
    if(body.user){
      prizeHistoryUpdate.user = body.user;
    }
    if(body.prize){
      prizeHistoryUpdate.prize = body.prize;
    }
    PrizeHistory.findByIdAndUpdate(id,prizeHistoryUpdate,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(200).json({
        message: "Update PrizeHistory successfully",
        PrizeHistory: data
      })
    })
  }

  deletePrizeHistoryById(req,res,next){
  
    let id = req.params.historyId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'PrizeHistoryId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    PrizeHistory.findByIdAndDelete(id,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(204).json({
        message: "Delete PrizeHistory successfully",
      })
    })
    }
} 
module.exports = new PrizeHistoryController;