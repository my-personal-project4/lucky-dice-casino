const DiceHistory = require('../model/diceHistory');
//import thư viện mongoose
const mongoose = require('mongoose');

class DiceHistoryController {
  //
  getAllDiceHistory(req,res) {
    //B1: thu thập dữ liệu từ req
    // let id = req.query.user
    let {user} = req.query
    let condition = {};
    if(user){
        condition.user = user;
    }
    //B2: validate dữ liệu
    // if(!mongoose.Types.ObjectId.isValid(id)) {
    //   return res.status(400).json({
    //     message: 'UserId is invalid!'
    //   })
    // }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    DiceHistory.find(condition)
    .populate('user')
    .exec((error,data)=> {
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Get all DiceHistorys successfully",
        DiceHistories: data
      })
    })
  }

  createDiceHistory(req,res){
    //B1: thu thập dữ liệu từ req
    let body = req.body;
    // console.log(body);
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
      return res.status(400).json({
        message: 'User is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newDiceHistoryData = {
      _id: mongoose.Types.ObjectId(),
      user: body.user,
    }
    DiceHistory.create(newDiceHistoryData, (error,data) =>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(201).json({
        message: "Create successfully",
        newDiceHistory: data
      })
    })
  }

  getDiceHistoryById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.diceHistoryid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'DiceHistoryId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    DiceHistory.findById(id)
    .populate('user')
    .exec((error,data)=> {
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(200).json({
        message: "Get DiceHistory successfully",
        DiceHistory: data
      })
    })
  }

  updateDiceHistoryById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.diceHistoryid;
    let body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'DiceHistoryId is invalid!'
      })
    }
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
      return res.status(400).json({
        message: 'User is invalid!'
      })
    }
    //bóc tách trường hợp undefined
    if(body.dice !== undefined &&(!Number.isInteger(body.dice) || body.dice < 0)){
      return res.status(400).json({
        message: 'Dice is invalid!'
      }) 
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let DiceHistoryUpdate = {
      user: body.user,
      dice: body.dice
    }
    if(body.user){
      DiceHistoryUpdate.user = body.user;
    }
    if(body.dice){
      DiceHistoryUpdate.dice = body.dice;
    }
    DiceHistory.findByIdAndUpdate(id,DiceHistoryUpdate,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Update DiceHistory successfully",
        DiceHistory: data
      })
    })
  }

  deleteDiceHistoryById(req,res,next){
  
    let id = req.params.diceHistoryid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'DiceHistoryId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    DiceHistory.findByIdAndDelete(id,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(204).json({
        message: "Delete DiceHistory successfully",
      })
    })
    }
} 
module.exports = new DiceHistoryController;