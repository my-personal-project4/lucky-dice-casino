const Prize = require('../model/Prize');

//import thư viện mongoose
const mongoose = require('mongoose');


class PrizeController {
  getAllPrize(req,res) {
    Prize.find()
      .then(Prizes => res.status(200).json(Prizes))
      .catch(error=>{
        res.status(500).json({
          message: error.message
        })
      });
  }
 
  createPrize(req,res){
    //B1: thu thập dữ liệu từ req
    let body = req.body;
    //B2: validate dữ liệu
    if(!body.name) {
      return res.status(400).json({
        message: 'name is required'
      })
    }
  
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newPrizeData = {
      _id: mongoose.Types.ObjectId(),
      name: body.name,
      description: body.description,
    }
    Prize.create(newPrizeData, (error,data) =>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      return res.status(201).json({
        message: "Create successfully",
        newPrize: data
      })
    })
  }

  getPrizeById(req,res){
    // console.log(req.params.Prizeid);
    //B1: thu thập dữ liệu từ req
    let id = req.params.prizeid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'PrizeId is invalid!'
      })
    }
    Prize.findById(id)
      .then ((Prize)=> res.status(200).json({
        message: "Get Prize successfully",
        Prize
      }))
      .catch(error=>{
        res.status(500).json({
          message: error.message
        })
      });
  }

  updatePrizeById(req,res){
    //B1: thu thập dữ liệu từ req
    let id = req.params.prizeid;
    let body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'PrizeId is invalid!'
      })
    }
    //bóc tách trường hợp undefined
    if(body.name !== undefined && body.name =="") {
      return res.status(400).json({
        message: 'name is required'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let PrizeUpdate = {
      name: body.name,
      description: body.description,
    }
    if(body.name){
      PrizeUpdate.name = body.name;
    }
    if(body.description){
      PrizeUpdate.description = body.description;
    }
    Prize.findByIdAndUpdate(id,PrizeUpdate,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Update Prize successfully",
        Prize: data
      })
    })
  }

  deletePrizeById(req,res){
    //B1: Chuẩn bị dữ liệu
    let PrizeId = req.params.prizeid;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(PrizeId)) {
      return req.status(400).json({
        status: "Error 400: Bad Request",
        message: "Prize ID is not valid"
      })
    }
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    Prize.findByIdAndDelete(PrizeId, (error) => {
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(204).json({
        message: "Delete Prize successfully",
      })
    })
    } 
}
module.exports = new PrizeController;